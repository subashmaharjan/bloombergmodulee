/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.constants;

/**
 *
 * @author Subash
 */
public enum Status {
    DELETED,
    PENDING,
    COMPLETED;

    private Status() {
    }

}
