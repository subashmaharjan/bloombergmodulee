/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Subash
 */
@Entity
@Table(name = "DEAL")
public class Deals implements Serializable {

    @Id
    private String dealId;

    @Column(nullable = false, name = "ORDERING_CURRENCY_CODE")
    private String orderingCurrencyCode;

    @Column(nullable = false, name = "TO_CURRENCY_CODE")
    private String toCurrencyCode;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "DEAL_TIME", nullable = false)
    private Date dealTime;

    @Column(name = "DEAL_AMOUNT_IN_ORDERING_CURRENCY", nullable = false)
    private Double dealAmountInOrderingCurrency;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false)
    private Source source;

    public Deals() {
    }

    public Deals(String dealId, String orderingCurrencyCode, String toCurrencyCode, Date dealTime, Double dealAmountInOrderingCurrency) {
        this.dealId = dealId;
        this.orderingCurrencyCode = orderingCurrencyCode;
        this.toCurrencyCode = toCurrencyCode;
        this.dealTime = dealTime;
        this.dealAmountInOrderingCurrency = dealAmountInOrderingCurrency;
    }
    
    

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getOrderingCurrencyCode() {
        return orderingCurrencyCode;
    }

    public void setOrderingCurrencyCode(String orderingCurrencyCode) {
        this.orderingCurrencyCode = orderingCurrencyCode;
    }

    public String getToCurrencyCode() {
        return toCurrencyCode;
    }

    public void setToCurrencyCode(String toCurrencyCode) {
        this.toCurrencyCode = toCurrencyCode;
    }

    public Date getDealTime() {
        return dealTime;
    }

    public void setDealTime(Date dealTime) {
        this.dealTime = dealTime;
    }

    public Double getDealAmountInOrderingCurrency() {
        return dealAmountInOrderingCurrency;
    }

    public void setDealAmountInOrderingCurrency(Double dealAmountInOrderingCurrency) {
        this.dealAmountInOrderingCurrency = dealAmountInOrderingCurrency;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    
    
}
