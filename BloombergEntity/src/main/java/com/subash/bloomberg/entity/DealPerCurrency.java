/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.entity;

import java.io.Serializable;
import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Subash
 */
@Entity
@Table(name = "DEAL_PER_CURRENCY")
public class DealPerCurrency implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CURRENCY_ISO_CODE", nullable = false, unique = true)
    private String currencyIsoCode;
    @Column(name = "TOTAL_DEAL", nullable = false)
    private Long totalDeal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public Long getTotalDeal() {
        return totalDeal;
    }

    public void setTotalDeal(Long totalDeal) {
        this.totalDeal = totalDeal;
    }

}
