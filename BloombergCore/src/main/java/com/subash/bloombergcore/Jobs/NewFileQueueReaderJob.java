/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.Jobs;

import com.subash.bloombergcore.services.implementation.LocalFileReader;
import com.subash.bloombergcore.singletons.BloombergFileQueue;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Subash
 */
public class NewFileQueueReaderJob implements Runnable {
    
    private final Logger logger = LoggerFactory.getLogger(NewFileAndQueueJob.class);
    
    @Override
    public void run() {
        logger.info("Queue job has been Started");
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(5);
        while (true) {
            
            try {
                while (!BloombergFileQueue.getInstance().isEmpty()) {
                    String file = BloombergFileQueue.getInstance().Dequeue();
                    FileWriterJob fileWriterJob = new FileWriterJob(file, new LocalFileReader());
                    logger.info("Running FileWriter Job with {}", file);
                    newFixedThreadPool.submit(fileWriterJob);
                }
                Thread.sleep(100);
            } catch (InterruptedException e) {
                newFixedThreadPool.shutdown();              
                System.exit(0);
            } catch (Exception e) {
                logger.error("error", e);
            }
        }
    }
    
}
