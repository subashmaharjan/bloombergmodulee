/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.Jobs;

import com.subash.bloombergcore.singletons.BloombergEMF;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Subash
 */
public class BatchWriterJob<T> implements Runnable {

    Logger logger = LoggerFactory.getLogger(BatchWriterJob.class);
    private final List<T> batch;
    boolean sucess = false;
    int count = 0;

    public BatchWriterJob(List<T> batch) {
        this.batch = batch;
    }

    @Override
    public void run() {
        logger.info("Running Batch");
        while (count < 5 && !sucess) {
            persist();
        }
        logger.info("Batch Completd");
    }

    public void persist() {
        EntityManager em = BloombergEMF.getInstance().getEntityManager();
        try {

            em.getTransaction().begin();
            batch.stream().forEach((t) -> {
                em.merge(t);
            });
            em.getTransaction().commit();
            sucess = true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            logger.error("error", e);
            count++;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
            }

        } finally {
//            em.close();
        }
    }

}
