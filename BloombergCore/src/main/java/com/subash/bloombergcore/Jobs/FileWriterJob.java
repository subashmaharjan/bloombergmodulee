/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.Jobs;

import com.subash.bloomberg.constants.Status;
import com.subash.bloomberg.entity.Deals;
import com.subash.bloomberg.entity.InValidData;
import com.subash.bloomberg.entity.Source;
import com.subash.bloombergcore.dao.SourceDAO;
import com.subash.bloombergcore.dao.DealPerCurrencyDAO;
import com.subash.bloombergcore.dao.implemenation.SourceDAOImpl;
import com.subash.bloombergcore.dao.implemenation.DealPerCurrencyDAOImpl;
import com.subash.bloombergcore.exception.FileNotFoundException;
import com.subash.bloombergcore.exception.InvalidDataException;
import java.io.BufferedReader;

import java.io.IOException;
import org.slf4j.LoggerFactory;
import com.subash.bloombergcore.services.FileReaderWriter;
import com.subash.bloombergcore.singletons.ApplicationProperty;
import com.subash.bloombergcore.validator.DataValidator;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.slf4j.Logger;

/**
 *
 * @author Subash
 */
public class FileWriterJob implements Runnable {

    private final String file;
    private final FileReaderWriter fileReaderWriter;
    public static Logger logger = LoggerFactory.getLogger(FileWriterJob.class);
    private final SourceDAO sourceDAO = new SourceDAOImpl();
    private final DealPerCurrencyDAO dealPerCurrencyDAO = new DealPerCurrencyDAOImpl();
    ExecutorService newFixedThreadPool = Executors.newCachedThreadPool();

    public FileWriterJob(String file, FileReaderWriter fileReaderWriter) {
        this.fileReaderWriter = fileReaderWriter;
        this.file = file;
    }

    @Override
    public void run() {
        logger.info("Intiating writing Jobs {} file", file);
        long start = System.currentTimeMillis();
        try {
            String location = ApplicationProperty.getInstance().getProperty("localfilelocation");
            Source saveFile = saveFile();
            if (saveFile == null) {
                return;
            }
            FileReader fileReader = fileReaderWriter.getFile(file, location);
            try (BufferedReader br = new BufferedReader(fileReader);) {
                saveAndValidateData(br, saveFile);
                updateFileStatus(saveFile);
            } catch (FileNotFoundException | IOException ex) {

            }
            fileReaderWriter.moveFile(file, location);
        } catch (Exception e) {
            logger.error("error", e);
        }
        logger.info("Total Time taken for {} file is {}", file, (System.currentTimeMillis() - start) / 1000);
    }

    private void saveAndValidateData(BufferedReader br, Source source) throws InterruptedException {
        List<Deals> validDeals = new ArrayList<>();
        List<InValidData> inValidDeals = new ArrayList<>();
        logger.info("valididating data");
        br.lines().forEach((data) -> {
            try {
                Deals validDeal = DataValidator.getValidDeal(data);
                validDeal.setSource(source);
                validDeals.add(validDeal);
            } catch (InvalidDataException ex) {
                InValidData inValidData = new InValidData();
                inValidData.setInValidData(data);
                inValidData.setSource(source);
                inValidDeals.add(inValidData);
            }
        });
        submitBatch(validDeals);
        submitBatch(inValidDeals);
        newFixedThreadPool.shutdown();
        newFixedThreadPool.awaitTermination(5, TimeUnit.MINUTES);
        increaseCurrencyCodeCount(validDeals);
    }

    private void increaseCurrencyCodeCount(List<Deals> validDeals) {

        dealPerCurrencyDAO.increaseCount(getCountPerCurrency(validDeals));
    }

    public Map<String, Long> getCountPerCurrency(List<Deals> validDeals) {
        return validDeals.stream().collect(Collectors.groupingBy(Deals::getOrderingCurrencyCode, Collectors.counting()));
    }

    private <T> void submitBatch(List<T> wholeData) {
        List<T> batch = new ArrayList<>();
        int count = 0;
        for (T t : wholeData) {
            batch.add(t);
            if (count % 5000 == 0) {
                BatchWriterJob<T> batchWriterJob = new BatchWriterJob<>(batch);
                newFixedThreadPool.submit(batchWriterJob);
                batch = new ArrayList<>();
            }
            count++;
        }
        BatchWriterJob<T> batchWriterJob = new BatchWriterJob<>(batch);
        newFixedThreadPool.submit(batchWriterJob);

    }

    private void updateFileStatus(Source source) {
        source.setStatus(Status.COMPLETED);
        source.setEndDate(new Date());
        sourceDAO.updateFile(source);
    }

    private Source saveFile() {
        if (sourceDAO.isFileRead(file)) {
            return null;
        }
        Source source = new Source();
        source.setFilename(file);
        source.setStatus(Status.PENDING);
        source.setStartDate(new Date());
        if (sourceDAO.saveFile(source)) {
            return source;
        }
        return null;
    }

}
