/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.Jobs;

import com.subash.bloombergcore.dao.SourceDAO;
import com.subash.bloombergcore.services.implementation.LocalFileReader;
import com.subash.bloombergcore.singletons.ApplicationProperty;
import com.subash.bloombergcore.singletons.BloombergFileQueue;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.subash.bloombergcore.services.FileReaderWriter;

/**
 *
 * @author Subash
 */
public class NewFileAndQueueJob implements Runnable {

    private final FileReaderWriter fileReader;
    private final Logger logger = LoggerFactory.getLogger(NewFileAndQueueJob.class);
    SourceDAO sourceDAO;

    public NewFileAndQueueJob( FileReaderWriter fileReader, SourceDAO sourceDAO) {
        this.fileReader = fileReader;
        this.sourceDAO = sourceDAO;
    }

    @Override
    public void run() {
        try {
            logger.info("Searching file in Location");
            String location = ApplicationProperty.getInstance().getProperty("localfilelocation");

            List<String> allAvailiableCSVFiles = fileReader.getAllAvailiableCSVFiles(location);
            logger.info("files {} found", allAvailiableCSVFiles.size());
            allAvailiableCSVFiles.forEach((e) -> {

                if (!sourceDAO.isFileRead(e)) {
                    logger.info("Submitting file for processing");
                    BloombergFileQueue.getInstance().Queue(e);

                }
            });
        } catch (Exception e) {
            logger.error("Error", e);
        }

    }

}
