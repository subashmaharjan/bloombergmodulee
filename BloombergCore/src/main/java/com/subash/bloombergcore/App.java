 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore;

import com.subash.bloombergcore.Jobs.NewFileAndQueueJob;
import com.subash.bloombergcore.Jobs.NewFileQueueReaderJob;
import com.subash.bloombergcore.dao.SourceDAO;
import com.subash.bloombergcore.dao.implemenation.SourceDAOImpl;
import com.subash.bloombergcore.services.implementation.LocalFileReader;
import com.subash.bloombergcore.singletons.ApplicationProperty;
import com.subash.bloombergcore.singletons.BloombergEMF;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.Scanner;

/**
 *
 * @author Subash
 */
public class App {

    public static void main(String[] args) {
        bootStrap();
        Long delay = Long.parseLong(ApplicationProperty.getInstance().getProperty("newFileJobTime"));
        ScheduledExecutorService scheduled = Executors.newSingleThreadScheduledExecutor();
        Supplier<NewFileAndQueueJob> supplier = () -> new NewFileAndQueueJob(new LocalFileReader(), new SourceDAOImpl());
        scheduled.scheduleAtFixedRate(supplier.get(), 0, delay, TimeUnit.SECONDS);
        Thread thread = new Thread(new NewFileQueueReaderJob());
        thread.start();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String nextLine = scanner.nextLine();
            if (nextLine.equalsIgnoreCase("Quit")) {
                scheduled.shutdown();
                thread.interrupt();
                shutDown();
                break;
            }
        }
    }

    public static void shutDown() {
        BloombergEMF.getInstance().close();
    }

    public static void bootStrap() {
        for (int i = 0; i < 100; i++) {
            System.out.println("Type Quit to exit");
        }

        BloombergEMF.getInstance();
        ApplicationProperty.getInstance();
        BloombergEMF.getInstance();
        SourceDAO sourceDAOImpl = new SourceDAOImpl();
        sourceDAOImpl.deletePending();
    }
}
