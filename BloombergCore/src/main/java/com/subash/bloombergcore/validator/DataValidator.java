/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.validator;

import com.subash.bloomberg.entity.Deals;
import com.subash.bloombergcore.exception.InvalidDataException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Subash
 */
public class DataValidator {

    public static Deals getValidDeal(String realData) {
        String[] datas = realData.split(",");
        if (datas.length != 5) {
            throw new InvalidDataException();
        }
        Deals deal = new Deals();
        deal.setDealId(datas[0]);
        deal.setDealAmountInOrderingCurrency(getAmount(datas[1]));
        deal.setDealTime(getDate(datas[2], "yyyy-MM-dd hh:mm:ss"));
        deal.setOrderingCurrencyCode(datas[3]);
        deal.setToCurrencyCode(datas[4]);
        return deal;
    }

    public static double getAmount(String data) {
        try {
            
            return Double.parseDouble(data);
        } catch (NumberFormatException e) {
            throw new InvalidDataException();
        }
    }

    public static Date getDate(String date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        try {
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new InvalidDataException();
        }
    }

}
