/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.singletons;

import ch.qos.logback.classic.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Subash
 */
public class ApplicationProperty {

    private Properties props;

    public ApplicationProperty() {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();

        try (InputStream ios = classloader.getResourceAsStream("application.properties");) {
            props = new Properties();
            props.load(ios);

        } catch (Exception ex) {
            LoggerFactory.getLogger(ApplicationProperty.class).error("Rrror Logding application Properties", ex);
        }

    }

    public static ApplicationProperty getInstance() {
        if (INSTANCE == null) {
            synchronized (ApplicationProperty.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ApplicationProperty();
                }
            }
        }
        return INSTANCE;
    }

    private static ApplicationProperty INSTANCE;

    public String getProperty(String key) {
        return props.getProperty(key);
    }
    public Properties getProperties(){
        return props;
    }

}
