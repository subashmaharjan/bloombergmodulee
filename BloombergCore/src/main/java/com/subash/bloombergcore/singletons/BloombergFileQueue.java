/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.singletons;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author Subash
 */
public class BloombergFileQueue {

    private final BlockingQueue<String> queue;
    private static volatile BloombergFileQueue INSTANCE;

    private BloombergFileQueue() {
        queue = new ArrayBlockingQueue<>(10);
    }

    public static BloombergFileQueue getInstance() {
        if (INSTANCE == null) {
            synchronized (BloombergFileQueue.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BloombergFileQueue();
                }
            }
        }
        return INSTANCE;
    }

    public void Queue(String o) {
        try {
            queue.put(o);
        } catch (InterruptedException e) {

        }

    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public String Dequeue() throws InterruptedException {
        return queue.take();
    }

    public int size() {
        return queue.size();
    }

}
