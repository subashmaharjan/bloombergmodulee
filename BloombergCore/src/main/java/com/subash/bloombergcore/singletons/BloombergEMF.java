/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.singletons;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Subash
 */
public class BloombergEMF {

    private final EntityManagerFactory entityManagerFactory;

    private BloombergEMF() {
        ApplicationProperty.getInstance().getProperties();
       
        entityManagerFactory = Persistence.createEntityManagerFactory("Bloomberg_PU",ApplicationProperty.getInstance().getProperties());
    }

    public static BloombergEMF getInstance() {
        if (INSTANCE == null) {
            synchronized (BloombergEMF.class) {
                if (INSTANCE == null) {
                    INSTANCE = new BloombergEMF();
                }
            }
        }
        return INSTANCE;
    }

    private static BloombergEMF INSTANCE;

    public void close() {
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }
    }
    
    public EntityManager getEntityManager(){
        return entityManagerFactory.createEntityManager();
    }

}
