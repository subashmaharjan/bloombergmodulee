/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.dao.implemenation;

import com.subash.bloomberg.constants.Status;
import com.subash.bloomberg.entity.Source;
import com.subash.bloombergcore.dao.SourceDAO;
import com.subash.bloombergcore.singletons.BloombergEMF;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Subash
 */
public class SourceDAOImpl implements SourceDAO {

    Logger logger = LoggerFactory.getLogger(SourceDAOImpl.class);

    public SourceDAOImpl() {
    }

    private EntityManager getEntityManager() {
        return BloombergEMF.getInstance().getEntityManager();
    }

    @Override
    public boolean isFileRead(String file) {
        try {
            Object singleResult = getEntityManager().createQuery("SELECT s from Source s where s.filename=:fileName and s.status!=:status")
                    .setParameter("fileName", file)
                    .setParameter("status", Status.DELETED)
                    .setMaxResults(1)
                    .getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public boolean saveFile(Source source) {
        try {
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(source);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public boolean updateFile(Source source) {
        try {
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.merge(source);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public boolean deletePending() {
        try {
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            int executeUpdate = entityManager.createQuery("UPDATE Source s SET s.status=:delete WHERE s.status=:status" )
                    .setParameter("status", Status.PENDING)
                    .setParameter("delete", Status.DELETED)
                    .executeUpdate();
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            logger.error("Error while deleing", e);
        }
        return false;
    }

}
