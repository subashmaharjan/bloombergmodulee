/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.dao.implemenation;

import com.subash.bloomberg.entity.Source;
import com.subash.bloombergcore.dao.SourceDAO;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author user
 */
public class DummpySourceDAOImpl implements SourceDAO {

    public Map<String, String> readFiles;

    public DummpySourceDAOImpl() {
        readFiles = new HashMap<>();
        readFiles = new HashMap<>();
        readFiles.put("2.csv", "2.csv");
        readFiles.put("3.csv", "3.csv");
        readFiles.put("4.csv", "4.csv");
        readFiles.put("5.csv", "5.csv");
    }

    @Override
    public boolean isFileRead(String file) {
        return readFiles.containsKey(file);
    }

    @Override
    public boolean saveFile(Source source) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean updateFile(Source source) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deletePending() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
