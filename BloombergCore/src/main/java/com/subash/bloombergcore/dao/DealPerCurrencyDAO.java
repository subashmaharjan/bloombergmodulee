/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.dao;

import com.subash.bloomberg.entity.DealPerCurrency;
import com.subash.bloomberg.entity.Deals;
import java.util.Map;

/**
 *
 * @author user
 */
public interface DealPerCurrencyDAO {

    public void increaseCount(Map<String, Long> dealPerCurrency);

    public boolean save(DealPerCurrency dealPerCurrency);

    public boolean update(DealPerCurrency dealPerCurrency);

    public DealPerCurrency getDealPerCurrencyByCurrencyCode(String currencyCode);

}
