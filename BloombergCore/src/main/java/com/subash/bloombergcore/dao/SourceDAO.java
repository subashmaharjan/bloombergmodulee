/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.dao;

import com.subash.bloomberg.entity.Source;
import java.util.List;

/**
 *
 * @author Subash
 */
public interface SourceDAO {

    public boolean isFileRead(String file);

    public boolean saveFile(Source source);

    public boolean updateFile(Source source);

    public boolean deletePending();

}
