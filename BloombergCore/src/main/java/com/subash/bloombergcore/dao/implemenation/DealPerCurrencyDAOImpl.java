/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.dao.implemenation;

import com.subash.bloomberg.entity.DealPerCurrency;
import com.subash.bloomberg.entity.Deals;
import com.subash.bloombergcore.dao.DealPerCurrencyDAO;
import com.subash.bloombergcore.singletons.BloombergEMF;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author user
 */
public class DealPerCurrencyDAOImpl implements DealPerCurrencyDAO {

    Logger logger = LoggerFactory.getLogger(DealPerCurrencyDAOImpl.class);

    public DealPerCurrencyDAOImpl() {
    }

    private EntityManager getEntityManager() {
        return BloombergEMF.getInstance().getEntityManager();
    }

    @Override
    public void increaseCount(Map<String, Long> dealPerCurrencies) {

        for (Map.Entry<String, Long> entry : dealPerCurrencies.entrySet()) {
            try {
                DealPerCurrency dealPerCurrency = this.getDealPerCurrencyByCurrencyCode(entry.getKey());
                dealPerCurrency.setTotalDeal(dealPerCurrency.getTotalDeal() + entry.getValue());
                this.update(dealPerCurrency);
            } catch (NoResultException ex) {
                DealPerCurrency dealPerCurrency = new DealPerCurrency();
                dealPerCurrency.setCurrencyIsoCode(entry.getKey());
                dealPerCurrency.setTotalDeal(entry.getValue());
                this.save(dealPerCurrency);

            }
        }

    }

    @Override
    public boolean save(DealPerCurrency dealPerCurrency) {
        try {
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.persist(dealPerCurrency);
            entityManager.getTransaction().commit();
            return true;
        } catch (PersistenceException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                DealPerCurrency persisted = this.getDealPerCurrencyByCurrencyCode(dealPerCurrency.getCurrencyIsoCode());
                persisted.setTotalDeal(persisted.getTotalDeal() + dealPerCurrency.getTotalDeal());
                return this.update(persisted);
            }
            throw e;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }

        return false;
    }

    @Override
    public boolean update(DealPerCurrency dealPerCurrency) {
        try {
            EntityManager entityManager = getEntityManager();
            entityManager.getTransaction().begin();
            entityManager.merge(dealPerCurrency);
            entityManager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            logger.error("Error while persisting", e);
        }
        return false;
    }

    @Override
    public DealPerCurrency getDealPerCurrencyByCurrencyCode(String currencyCode) {
        EntityManager em = getEntityManager();

        return em.createQuery("Select dpc FROM DealPerCurrency dpc where dpc.currencyIsoCode=:code", DealPerCurrency.class)
                .setParameter("code", currencyCode)
                .getSingleResult();

    }
}
