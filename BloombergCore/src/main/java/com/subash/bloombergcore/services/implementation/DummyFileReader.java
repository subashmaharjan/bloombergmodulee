/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.services.implementation;

import com.subash.bloombergcore.services.FileReaderWriter;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author user
 */
public class DummyFileReader implements FileReaderWriter {

    public List<String> avaliableFiles;
  

    public DummyFileReader() {
        avaliableFiles = Arrays.asList(
                "1.csv",
                "2.csv",
                "3.csv",
                "4.csv",
                "5.csv",
                "6.csv",
                "7.csv",
                "8.csv"
        );

    
    }

    @Override
    public List<String> getAllAvailiableCSVFiles(String location) {
        return avaliableFiles;
    }

    @Override
    public void moveFile(String file, String location) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public FileReader getFile(String fileName, String location) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
