/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.services.implementation;

import com.subash.bloombergcore.exception.FileNotFoundException;
import java.io.File;

import java.util.Arrays;
import java.util.List;
import com.subash.bloombergcore.services.FileReaderWriter;
import com.subash.bloombergcore.singletons.ApplicationProperty;
import java.io.FileReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Subash
 */
public class LocalFileReader implements FileReaderWriter {

    Logger logger = LoggerFactory.getLogger(LocalFileReader.class);

    @Override
    public List<String> getAllAvailiableCSVFiles(String location) {
        File file = new File(location);
        if (!file.exists()) {
            throw new FileNotFoundException("Location Not Found. Please check Location");
        }
        return Stream.of(file.listFiles((dir, name) -> name.toLowerCase().endsWith(".csv"))).map(File::getName).collect(Collectors.toList());

    }

    @Override
    public void moveFile(String fileName, String location) {
        try {

            String moveLocation = location + ApplicationProperty.getInstance().getProperty("completedFolder") + fileName;
            Path orginal = FileSystems.getDefault().getPath(location + fileName);
            Path move = FileSystems.getDefault().getPath(moveLocation);
            Files.move(orginal, move, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            logger.error("error", e);
        }
    }

    @Override
    public FileReader getFile(String fileName, String location) {
        File file = new File(location + fileName);
        try {
            return new FileReader(file);
        } catch (java.io.FileNotFoundException ex) {
            throw new FileNotFoundException("file has been moved");
        }
    }

}
