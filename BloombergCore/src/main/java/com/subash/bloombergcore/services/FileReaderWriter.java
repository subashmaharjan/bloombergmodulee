/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.services;

import java.io.File;
import java.io.FileReader;
import java.util.List;

/**
 *
 * @author Subash
 */
public interface FileReaderWriter {

    public List<String> getAllAvailiableCSVFiles(String location);

    public void moveFile(String file, String location);

    public FileReader getFile(String fileName, String location);

}
