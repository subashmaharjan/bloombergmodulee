/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.test;

import com.subash.bloomberg.entity.Deals;
import com.subash.bloombergcore.Jobs.FileWriterJob;
import com.subash.bloombergcore.services.implementation.LocalFileReader;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class FileWriterJobTest {

    FileWriterJob fileWriterJob = new FileWriterJob("dadsa", new LocalFileReader());
    public List<Deals> validDeals = Arrays.asList(
            new Deals("dd", "USD", "DD", new Date(), 1d),
            new Deals("dd", "INR", "DD", new Date(), 1d),
            new Deals("dd", "INR", "DD", new Date(), 1d),
            new Deals("dd", "INR", "DD", new Date(), 1d),
            new Deals("dd", "INR", "DD", new Date(), 1d),
            new Deals("dd", "NPR", "DD", new Date(), 1d),
            new Deals("dd", "NPR", "DD", new Date(), 1d),
            new Deals("dd", "NPR", "DD", new Date(), 1d),
            new Deals("dd", "USD", "DD", new Date(), 1d),
            new Deals("dd", "USD", "DD", new Date(), 1d)
    );

    public FileWriterJobTest() {
        
    }
    
    @Test
    public void TestCount(){
        Map<String, Long> countPerCurrency = fileWriterJob.getCountPerCurrency(validDeals);
        
        assertTrue(countPerCurrency.get("USD").equals(3l));
        assertTrue(countPerCurrency.get("INR").equals(4l));
        assertTrue(countPerCurrency.get("NPR").equals(3l));
    }

}
