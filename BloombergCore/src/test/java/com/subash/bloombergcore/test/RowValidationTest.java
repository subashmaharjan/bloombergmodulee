/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.test;

import com.subash.bloomberg.entity.Deals;
import com.subash.bloombergcore.exception.InvalidDataException;
import com.subash.bloombergcore.validator.DataValidator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class RowValidationTest {
    
    public RowValidationTest() {
    }
    
    @Test
    public void Test(){
        Deals validDeal = DataValidator.getValidDeal("deal10005,10005,2017-06-13 22:58:53,EUR1,EUR");
        assertNotNull(validDeal);
        assertEquals(validDeal.getDealId(), "deal10005");
        assertTrue(validDeal.getDealAmountInOrderingCurrency().equals(10005d));
        assertEquals(validDeal.getOrderingCurrencyCode(),"EUR1" );
        assertEquals(validDeal.getToCurrencyCode(),"EUR" );
        assertEquals(validDeal.getDealId(), "deal10005");
      
    }
    
    @Test(expected = InvalidDataException.class)
    public void TestDateInvalid(){
       DataValidator.getValidDeal("deal10005,10005,2017-13 22:58:53,EUR,EUR");
    }
    @Test(expected = InvalidDataException.class)
    public void TestROwInvalid(){
       DataValidator.getValidDeal("deal1000510005,2017-13 22:58:53,EUR,EUR");
    }
    
    @Test(expected = InvalidDataException.class)
    public void TestPrice(){
       DataValidator.getValidDeal("deal10005,10005ssss,2017-06-13 22:58:53,EUR,EUR");
    }
    
    
    
}
