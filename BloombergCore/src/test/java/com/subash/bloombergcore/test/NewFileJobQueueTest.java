/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloombergcore.test;

import com.subash.bloombergcore.Jobs.NewFileAndQueueJob;
import com.subash.bloombergcore.dao.SourceDAO;
import com.subash.bloombergcore.dao.implemenation.DummpySourceDAOImpl;
import com.subash.bloombergcore.dao.implemenation.SourceDAOImpl;
import com.subash.bloombergcore.services.implementation.DummyFileReader;
import com.subash.bloombergcore.services.implementation.LocalFileReader;
import com.subash.bloombergcore.singletons.BloombergFileQueue;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class NewFileJobQueueTest {

    NewFileAndQueueJob job;
    SourceDAO sourceDAO;

    public NewFileJobQueueTest() {
        sourceDAO = new DummpySourceDAOImpl();
        job = new NewFileAndQueueJob(new DummyFileReader(), sourceDAO);
    }

    @Test
    public void testOnlyUnreadFileInQueue() throws InterruptedException {
        job.run();
        for (int i = 0; i < BloombergFileQueue.getInstance().size(); i++) {
            assertFalse(sourceDAO.isFileRead(BloombergFileQueue.getInstance().Dequeue()));
        }

    }

}
