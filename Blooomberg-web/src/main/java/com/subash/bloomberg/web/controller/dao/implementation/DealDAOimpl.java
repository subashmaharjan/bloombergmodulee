/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.web.controller.dao.implementation;

import com.subash.bloomberg.entity.Deals;
import com.subash.bloomberg.web.controller.dao.DealDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

/**
 *
 * @author Subash
 */
@Component
public class DealDAOimpl implements DealDAO {
    
    @PersistenceContext(unitName = "bloomberg_PU")
    EntityManager em;
    
    @Override
    public List<Deals> getAllDealsByFileName(String fileName, Integer page) {
        return em.createQuery("Select d FROM Deals d where d.source.filename= :fileName")
                .setParameter("fileName", fileName)
                .setMaxResults(20)
                .setFirstResult(20 * (page-1))
                .getResultList();
    }
    
}
