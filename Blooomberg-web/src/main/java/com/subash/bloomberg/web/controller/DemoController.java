/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.web.controller;

import com.subash.bloomberg.entity.Deals;
import com.subash.bloomberg.singletons.ApplicationProperty;
import com.subash.bloomberg.web.controller.dao.DealDAO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Subash
 */
@Controller
@RequestMapping("/")
public class DemoController {

    @Autowired
    DealDAO dealDAO;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "uploadFile", method = RequestMethod.POST)
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes) {
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/";
        }
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(ApplicationProperty.getInstance().getProperty("localfilelocation") + file.getOriginalFilename());
            System.out.println(path);
            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/";
    }

    @RequestMapping(method = RequestMethod.GET, value = "search")
    public String search(@RequestParam(value = "query") String query,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            ModelMap modelMap
    ) {
        if (page <= 0) {
            page = 1;
        }
        List<Deals> allDealsByFileName = dealDAO.getAllDealsByFileName(query, page);
        System.out.println(allDealsByFileName.size());
        modelMap.put("query", query);
        modelMap.put("page", page);
        modelMap.put("deals", allDealsByFileName);
        return "index";
    }

}
