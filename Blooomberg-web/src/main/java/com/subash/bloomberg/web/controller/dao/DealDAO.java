/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.subash.bloomberg.web.controller.dao;

import com.subash.bloomberg.entity.Deals;
import java.util.List;

/**
 *
 * @author Subash
 */
public interface DealDAO {
    public List<Deals> getAllDealsByFileName(String fileName,Integer page );
}
