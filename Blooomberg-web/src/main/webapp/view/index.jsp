<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="demo">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.css"/>"/>
        <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-theme.min.css"/>"/>





    </head>
    <body class="container-fluid">
        <h1>${message}</h1>
        <div class="row">
            <div class="col-lg-6"> 

                <spring:url value="/search" var="search" htmlEscape="true"/>
                <form method="GET" action="${search}">
                    <div class="col-lg-3">
                        <input type="text" name="query"/>
                    </div>
                    <div class="col-lg-3">
                        <input type="submit" value="search"/>
                    </div>
                </form>

            </div>         
            <spring:url value="/uploadFile" var="upload" htmlEscape="true"/>
            <form method="POST" action="${upload}" enctype="multipart/form-data">

                <div class="col-lg-3">  <input type="file" name="file" accept=".csv"/>choses file</div>
                <div class="col-lg-3"><input type="submit" value="Upload"/> </div>
            </form>	
        </div>
        <c:if test="${not empty deals}">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>amount</th>
                        <th>date</th>
                        <th>From CurrencyCode</th>
                        <th>ToCurrencyCode</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${deals}" var="deal">
                        <tr>
                            <td>${deal.dealId}</td>
                            <td>${deal.dealAmountInOrderingCurrency}</td>
                            <td>${deal.dealTime}</td>
                            <td>${deal.orderingCurrencyCode}</td>
                            <td>${deal.toCurrencyCode}</td>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

    </body>
    <script type="text/javascript" src="<c:url value="/resources/javascript/jQuery-2.1.4.min.js"/>"></script>





</html>
